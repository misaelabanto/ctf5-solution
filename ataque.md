## 1. Subida de SHELL:
Descargamos de internet una reverse shell y la subimos como una página nueva en la web de eventos
Archivo: `php-reverse-shell.php`
URL generada: `http://192.168.43.193/events/?q=node/15`

**Comando ejecutado:** `nc -v -n -l -p 1234`


## 2. Linpeas
Descargamos el archivo linpeas al servidor, pero no logramos ejecutarlo. 

## 3. Escalando privilegios por la versión del Kernel
1. Buscamos la versión del Kernel (2.23.6) en exploit-db.com
2. Encontramos un código en C: Véase el archivo `woo2.c`
3. Al intentar compilar el código nos encontramos con las siguientes dificultades:
- La máquina víctima no tenía `gcc` ni ningún otro compilador de C.
- La máquina no permitía descargar archivos de algunas páginas con HTTPS. La solución era actualizar curl o wget, pero aún no disponíamos de root para tal tarea.
- La máquina víctima era de 32 bits, por lo que la compilación debía ser correspondiente a esta arquitectura.

Estos problemas fueron solucionados de la siguiente manera:
- Se usó la librería `gcc-multilib` en Kali Linux para compilar `woo2.c` en 32 bits.
- Se subió el compilado a un servidor de pruebas (testito) y con `python -m "SimpleHTTPServer"` se pudo servir la carpeta en donde se encontraba el archivo.
- Se descargó el archivo desde la shell de la máquina víctima.
- Se ejecutó varias veces (sucedían múltiples errores hasta que finalmente conseguimos root).
